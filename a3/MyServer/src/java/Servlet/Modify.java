package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.stream.Collectors;
import Login.test;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

@WebServlet(name = "Modify", urlPatterns = {"/Modify"})
public class Modify extends HttpServlet {

    public String username;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        res.setContentType("text/html");
        try (PrintWriter o = res.getWriter()) {

            String Query = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            //String[] tokens = Query.split("\\|", 2);
            //split("&",2);
            //username = tokens[0];
            //  Query = tokens[1];
            res.setContentType("text/html");

            if (Query.length() != 0) {
                /* de katafera na kanw get request, sinexeia m petouse error 405 zzzzzzzz*/

                String GetName = DecodeData(test.params, Query);

                // res.sendRedirect("LogedIn.html");
                //req.getRequestDispatcher("LogedIn.html").include(req, res);  
                /*  for (HashMap.Entry<String, HashMap<String, String>> entry : test.params.entrySet()) {

                    String key = entry.getKey();
                    o.println("@: " + key);

                    for (HashMap.Entry<String, String> var : entry.getValue().entrySet()) {
                        String key2 = var.getKey();
                        String value2 = var.getValue();
                        o.println("<br>");
                        o.println("---> " + key2 + " = " + value2);
                        o.println("<br>");
                    }
             

            }*/
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        res.setContentType("text/html");

        try (PrintWriter o = res.getWriter()) {
            o.println(test.print(1, test.currentusername));
        }
    }

    /**
     * @param data
     * @param str The queries from post method,we split them and put them in a
     * Map (key,value)
     * @return The Map
     */
    public String DecodeData(HashMap<String, HashMap<String, String>> data, String str) {
        //HashMap<String, HashMap<String, String>> data = new HashMap<String, HashMap<String, String>>();
        HashMap<String, String> user = new HashMap<>();

  
        String[] tokens = str.split("&");
        for (String t : tokens) {
            String[] parts = t.split("=");

            if (parts.length == 1) {
                user.put(parts[0], parts[1]);
            } else {

                user.put(parts[0], parts[1]);
            }

        }
   
        String mail = data.get(user.get("username")).get("email");
        
        user.put("email", mail);
        //String username=data.get("username").get("email");

        data.put(user.get("username"), user);

        return user.get("username");
    }

}
