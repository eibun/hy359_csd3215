package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.stream.Collectors;
import Login.test;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

@WebServlet(name = "Echo", urlPatterns = {"/Echo"})
public class Echo extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        String Query = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

        res.setContentType("text/html");

        if (test.params == null) {
            test.params = new HashMap<>();
        }

        try (PrintWriter o = res.getWriter()) {
            test.currentusername = null;
            o.println("<script>alert(2);</script>");
            if (Query.length() != 0) {
                /* de katafera na kanw get request, sinexeia m petouse error 405 zzzzzzzz*/
                if (test.params != null) {
                    String GetName = DecodeData(test.params, Query);

                    // res.sendRedirect("LogedIn.html");
                    //req.getRequestDispatcher("LogedIn.html").include(req, res);  
                    //o.println("javascript:alert(1)");
                    for (HashMap.Entry<String, HashMap<String, String>> entry : test.params.entrySet()) {

                        /* String key = entry.getKey();
                        o.println("@: " + key);

                        for (HashMap.Entry<String, String> var : entry.getValue().entrySet()) {
                            String key2 = var.getKey();
                            String value2 = var.getValue();
                            o.println("<br>");
                            o.println("---> " + key2 + " = " + value2);
                            o.println("<br>");
                        }*/
                        test.currentusername = entry.getKey();
                    }
                    o.println(test.params.get(test.currentusername).get("info"));
                }
            } else {
                /* remove cookies! */
                CookieExists(res, req.getCookies());
            }
        }
    }

    /**
     * @param data
     * @param str The queries from post method,we split them and put them in a
     * Map (key,value)
     * @return The Map
     */
    public String DecodeData(HashMap<String, HashMap<String, String>> data, String str) {
        //HashMap<String, HashMap<String, String>> data = new HashMap<String, HashMap<String, String>>();
        HashMap<String, String> user = new HashMap<>();

        int username = 0;
        String[] tokens = str.split("&");
        for (String t : tokens) {
            String[] parts = t.split("=");

            if (parts.length == 1) {
                user.put(parts[0], parts[1]);
            } else {

                user.put(parts[0], parts[1]);
            }

        }
        for (HashMap.Entry<String, HashMap<String, String>> entry : test.params.entrySet()) {
            for (HashMap.Entry<String, String> var : entry.getValue().entrySet()) {
                if (var.getKey().equals("email")) {

                    if (var.getValue().equals(user.get("email"))) {
                        System.out.println(user.get(("email")) + "already exists!");
                        return null;
                    }

                } else if (var.getKey().equals(user.get("username"))) {

                    /* username already exists in database! */
                    System.out.println(user.get(("username")) + "already exists!");

                    return null;
                }
            }
        }
        System.out.println("adding " + user.get("username"));
        data.put(user.get("username"), user);

        return user.get("username");
    }

    private void CookieExists(HttpServletResponse res, Cookie[] cookies) {
        if (cookies != null) {
            for (Cookie c : cookies) {
                c.setValue(null);
                c.setMaxAge(0);
                res.addCookie(c);
            }
        }
        System.out.println("Cookies Removed");
    }
}
