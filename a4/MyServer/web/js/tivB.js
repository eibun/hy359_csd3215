
var GPSlon = [];
var GPSlat = [];
var ExifData = [];
var ImageCount = 0;
var BiggerImage = 0;
var validFileType = ".jpg , .png , .bmp";
var CollapseButton = 0; /* not created */
var info =
	"<button  class=button id=toggleButton type=\"button\" onclick=toggleText()>Click Me!<\/button>";
var status = "more";
var currentIndex = 0;
var l;
var i; /* counter */
/*General Functions*/

var toDecimal = function (number) {
    
    "use strict";
	return number[0].numerator + number[1].numerator / (60 * number[1].denominator) +
        number[2].numerator / (3600 * number[2].denominator);
};


function getExif(index) {
    
    "use strict";
	var element = "item" + index, img2, lon, lat,  allMetaData, allMetaDataSpan;
	img2 = document.getElementById(element);
	EXIF.getData(img2, function () {
        lon = toDecimal(EXIF.getTag(this, 'GPSLongitude'));
        lat = toDecimal(EXIF.getTag(this, 'GPSLatitude'));
		GPSlon[index] = lon;
		GPSlat[index] = lat;
		console.log(lon);
		console.log(lat);
		allMetaData = EXIF.getAllTags(this);
        allMetaDataSpan = document.getElementById("allMetaDataSpan");
		allMetaDataSpan.innerHTML = JSON.stringify(allMetaData,
				null, "\t");
	});
}


var TIVcsd3215 = (function () {
    "use strict";
	function internalloadImages() {
        
		var ImageArray = [], files, i, file, type;

		files = document.getElementById("images").files;
		for (i = 0; i < files.length; i = i + 1) {
			file = files[i];
			console.log(file);
			// Have to check that this is an image though
			// using  the file.name TODO
			type = file.name.substring(file.name.lastIndexOf('.'));
			if (validFileType.toLowerCase().indexOf(type) < 0) {
				console.log("Invalid File");
			} else {
				ImageCount = ImageCount + 1;
				ImageArray.push(file);
			}
		}
		return ImageArray;
	};

	function internalshowLoadedImages(elem) {

		var ImageArray = TIVcsd3215.getLoadedImages(), span, reader, array;

		if (ImageCount < 1) {

			span = document.createElement('div');
			span.setAttribute('id', 'BigText');
			span.innerHTML = ['<p>No images were found!</p>'].join('');
			document.getElementById(elem).insertBefore(span, null);
		} else {

			for (i = 0; i < ImageArray.length; i = i + 1) {

				reader = new FileReader();
				// Closure to capture the file information.
				reader.onload = (function (file) {
					/*Pass agruments into closure */
					var index = i;
					array = ImageArray;
					return function (e) {
						var span = document.createElement('div');
						span.setAttribute('class', 'tiv');
						span.href = e.target.result;
						span.setAttribute("onclick", 'TIVbcsd3215.showImage(\'' +
							index + '\',"container1")');

						span.innerHTML = ['<img  src="', e.target.result,
							'" title="', escape(file.name),
							'">'
                                         ].join('');
						file.x = e.target.result;
						file.y = escape(file.name);
						//file.z=file.webkitRelativePath;
						//alert(file.z);
						//console.log(getExif(file.z));
						document.getElementById(elem).insertBefore(span, null);
                        //var element = document.getElementById('container2').getElementsByClassName('tiv');
						//element.onclick=showImage(null,null);
					};
				})(ImageArray[i]);
				// Read in the image file as a data URL.
				reader.readAsDataURL(ImageArray[i]);
				//console.log(ImageArray[i]);
			}
		}
	};

	return {
		loadImages : function () {
			internalloadImages();
		},
		getLoadedImages : function () {
			return internalloadImages();
		},
		showLoadedImages : function () {
			internalshowLoadedImages('container2');
		},
		Run : function () {
			
			ImageCount = 0;
		    var elements = document.getElementsByClassName("tiv"), parent1, child, parent;
			while (elements.length > 0) {
				elements[0].parentNode.removeChild(elements[0]);
			}
			
			parent1 = document.getElementById('container2');
			child = document.getElementById("BigText");
			if (child !== null) {
				parent1.removeChild(child);
			}
			
			child = document.getElementById("map");
			if (child !== null) {
				child.innerHTML = '';
			}
			parent1 = document.getElementById('container1');
			child = document.getElementById("collapseb");
			if (child !== null) {
				parent1.removeChild(child);
			}
			
			
			GPSlon = [];
			GPSlat = [];
			ExifData = [];
			parent = document.getElementById('container1');
			if (BiggerImage === 1) {
			 
				child = document.getElementById("tiv2");
				parent.removeChild(child);
				BiggerImage = 0;
			}
			
			
			
			TIVcsd3215.loadImages();
			TIVcsd3215.getLoadedImages();
			TIVcsd3215.showLoadedImages('container2');
		}
	};

}());
var TIVbcsd3215 = (function () {
    "use strict";
    function internalshowImage(index, elem) {
        var ImageArray = TIVcsd3215.getLoadedImages(), ItemID, parent, child, span2, span;
		//var body = document.getElementsByTagName('body')[0];
		//body.style.backgroundImage = 'url('+ImageArray[index].x+')';
	
        ItemID = "item" + index;
		l = ItemID;
        parent = document.getElementById(elem);
        if (BiggerImage === 1) {
				
            child = document.getElementById("tiv2");
            parent.removeChild(child);
            BiggerImage = 0;
        }
		if (CollapseButton === 0) {
			/*CollapseButton created */
			span2 = document.createElement('div');
			span2.setAttribute('id', 'collapseb');
			span2.innerHTML = info;
			 
			document.getElementById(elem).insertBefore(span2, null);
			CollapseButton = 1;
		}
		document.getElementById("toggleButton").innerText = "See Less";
		status = "more";
		span = document.createElement('div');
		span.innerHTML = ['<img id ="', ItemID, '"src="', ImageArray[index].x,
			'" title="', ImageArray[index].y, '">'].join('');
		span.setAttribute('id', 'tiv2');
		span.innerHTML = span.innerHTML + ["<pre id=" + "\"allMetaDataSpan\"" +
				"></pre>"
			].join('');
		document.getElementById(elem).insertBefore(span, null);
		BiggerImage = 1;
		TIVbcsd3215.showImageDetailedExifInfo(index, elem);
	};

	

    function initMap(index, elem) {
        
        var uluru, map, marker;
        uluru = {
            lat : GPSlat[index],
            lng : GPSlon[index]
        };
        map = new google.maps.Map(document.getElementById('map'), {
            zoom : 4,
            center : uluru
        });
        marker = new google.maps.Marker({
            position : uluru,
            map : map
        });
    }
    
    function internalshowImageDetailedExifInfo(index, elem) {

		getExif(index);
		initMap();

		TIVbcsd3215.showImageDetailedExifWithMap(index, elem);
		ExifData[index] = document.getElementById("allMetaDataSpan").innerHTML;
	};
    
	function internalshowImageDetailedExifWithMap(index, elem) {
		initMap(index, elem);
		currentIndex = index;
	};

	
	
	return {
		showImage : function (index, elem) {
			internalshowImage(index, elem);
		},
		showImageDetailedExifInfo : function (index, elem) {
			internalshowImageDetailedExifInfo(index, elem);
		},
		showImageDetailedExifWithMap : function (index, elem) {
			internalshowImageDetailedExifWithMap(index, elem);
        }
	};
	
	
	
}());


function toggleText() {
    "use strict";
	if (status === "less") {
		document.getElementById("allMetaDataSpan").innerHTML = ExifData[currentIndex];
		document.getElementById("toggleButton").innerText = "See Less";
		status = "more";
	} else if (status === "more") {
		document.getElementById("allMetaDataSpan").innerHTML =
			"Click above to Show More";
		document.getElementById("toggleButton").innerText = "See More";
		status = "less";
	}
}
