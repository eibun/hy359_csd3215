package Login;

import java.util.HashMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nubie
 */
public class test {

    public static String currentusername;
    public static HashMap<String, HashMap<String, String>> params;

    /*
    
    mode = 0 prints only username and email for the signed up users
    mode !=0 prints all the fields of the hashmap
    
     */
    public static String print(int mode, String name) {
        String sb = "";
        if (mode == 0) {
            sb += ("<H1>Subscribed users</h1>");
            sb += ("<table border=\"1\">");
            sb += ("<tr>");
            sb += ("<td><b>Username</b></td>");
            sb += ("<td><b>Email</b></td>");
            sb += ("</tr>");
        }
        /*else if (mode == 1) {

            sb += ("<H1>Subscribed users</h1>");

            sb += ("<table border=\"1\">");
            sb += ("<tr>");
            sb += ("<td><b>Username</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Email</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>First Name</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Last Name</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Birthday</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Country</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>City</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>info</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Password</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Gender</b></td>");
            sb += ("</tr>");

            sb += ("</table>");
            sb += ("</br>");

        }*/

        for (HashMap.Entry<String, HashMap<String, String>> entry : test.params.entrySet()) {
         
                if (mode == 0) {
                    sb += ("<tr>");
                    sb += ("<td>" + (entry.getValue().get("username")) + "</td>");

                    sb += ("<td>" + (entry.getValue().get("email")) + "</td>");
                    sb += ("</tr>");

                } else {
                       if (entry.getKey().equals(name)) {
                    sb += (" <H1>Subscribed users</h1>");
                    sb += ("<table border=\"2\">");
                    sb += ("<tr>");
                    sb += ("<td>" + "Username: " + (entry.getValue().get("username")) + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Email " + (entry.getValue().get("email")) + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "First Name: " + (entry.getValue().get("firstname")) + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Last Name: " + (entry.getValue().get("lastName")) + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Age: " + (entry.getValue().get("birthday")) + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Country: " + (entry.getValue().get("country")) + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "City: " + (entry.getValue().get("city")) + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");

                    sb += ("<td>" + "Info: " + (entry.getValue().get("info")) + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Password: " + (entry.getValue().get("password")) + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Gender: " + (entry.getValue().get("gender")) + "</td>");
                    sb += ("</tr>");
                       
                }
            }
        }
        sb += ("</table>");
        sb += ("</br>");
        return sb;

    }
}
