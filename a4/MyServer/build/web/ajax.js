var error = 0;
function sendAjaxPOST() {
    error = 0;
    var username = CheckUserName();
    var fname = CheckName(3, 'fname');
    var lname = CheckName(4, 'lname');
    var email = CheckEmail();
    var password = CheckPassWord('password');
    var password2 = CheckPassWord('password2');
    if (password !== password2 || password === -1) {/* passwords match! */
        alert("Retype your Passwords!");
        error = -1;
    }
    var bday = CheckAge();

    var gender = findGender();

    var city = CheckCity();
    var country = document.getElementById("countries");
    country = country.options[country.selectedIndex].text;
    var info = document.getElementById('info').value;

    // alert(country);
    //alert(error);
    if (error < 0) {
        alert("Retype your information!");
    } else {
        alert("Your data are correct");


        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'Echo');
        xhr.onload = function () {
            if (xhr.status === 200) {

                document.getElementById('ajaxContent').innerHTML = xhr.responseText;
            } else if (xhr.status !== 200) {
                alert('Request failed. Returned status of ' + xhr.status);
            }
        };
        //alert(username);
        //xhr.send('username='+username  + '&lastName=' + lName);
        xhr.send('username=' + username + '&email=' + email +
                '&password=' + password + '&password2=' + password2 + '&firstname=' + fname +
                '&lastName=' + lname + '&birthday=' + bday + '&gender=' + gender + '&city=' + city + '&country=' + country + '&info=' + info);
    }
}
function MyInfo() {


    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'Modify');
    xhr.onload = function () {
        if (xhr.status === 200) {
            //alert(xhr.responseText);
            document.getElementById('myinfo')

                    .innerHTML = xhr.responseText;
        } else if (xhr.status !== 200) {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };

    xhr.send();


}




function EditUser() {
    error = 0;
    var fname = CheckName(3, 'fname');
    var lname = CheckName(4, 'lname');
    var password = CheckPassWord('password');
    var password2 = CheckPassWord('password2');
    if (password !== password2 || password === -1) {/* passwords match! */
        alert("Retype your Passwords!");
        error = -1;
    }
    var bday = CheckAge();

    var gender = findGender();

    var city = CheckCity();
    var country = document.getElementById("countries");
    country = country.options[country.selectedIndex].text;
    var info = document.getElementById('info').value;
    
    // alert(country);
    //alert(error);
    if (error < 0) {
        alert("Retype your information!");
    } else {
        alert("Your data are correct");


        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'Modify');
        xhr.onload = function () {
            if (xhr.status === 200) {
                document.getElementById('myinfo').innerHTML='';
                document.getElementById('ajaxContent').innerHTML = xhr.responseText;
            } else if (xhr.status !== 200) {
                alert('Request failed. Returned status of ' + xhr.status);
            }
        };
        //alert(username);
        //xhr.send('username='+username  + '&lastName=' + lName);

        xhr.send('username=' + localStorage.getItem('username') + '&password=' + localStorage.getItem('password') + '|&password=' + password + '&password2=' + password2 + '&firstname=' + fname +
                '&lastName=' + lname + '&birthday=' + bday + '&gender=' + gender + '&city=' + city + '&country=' + country + '&info=' + info);
    }
}








function findGender() {
    if (document.getElementById('male').checked) {
        return "male";
    } else if (document.getElementById('female').checked) {
        return "female";
    } else {
        return "na";
    }
}
function CheckUserName() {
    var username = document.getElementById('username').value;
    if (username.length <= 7) {
        alert("Give a username of at least 8 Characters!");
        error = -1;
        return -1;
    } else {
        return username;
    }


}
function CheckEmail() {
    var email = document.getElementById('email').value;
    var atcount = (email.match(/@/) || []).length;
    if (atcount === 1) {/*only 1 @ */
        var dotcount = (email.match(/./) || []).length;
        if (dotcount >= 1) {/*only 1 dot*/
            return email;
        } else
            alert("Give a Proper email address!");
    } else
        alert("Give a Proper email address!");
    error = -1;
}
function CheckPassWord(current) {
    var password = document.getElementById(current).value;
    if (password.length > 5 && password.length < 10) {/* valid length */

        if (/\d/.test(password)) {/*password contains a number */
            if (/[a-zA-Z]/.test(password)) {

                if (/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/.test(password))/* password contains special characters */
                    return password;
                else
                    alert("Password must contain at least one special symbol!");
            } else
                alert("Pass must contain chars!");
        } else {
            alert("Password must have a number!");
        }


    } else {
        error = -1;
        alert("Password Length must be > 6 and < 10")
        return -1;
    }
}
function CheckName(min, elem) {
    var field = document.getElementById(elem).value;
    if (field.length >= min && field.length < 20) {/* valid length */
        return field;
    } else {
        error = -1;
        if (min === 3) {
            alert("First name must be >3 and < 20");
        } else {
            alert("Last name must be >4 and < 20");
        }

        return -1;
    }


}
function CheckAge() {
    var bday = document.getElementById('birthday').value;
    var today = new Date();
    var birthDate = new Date(bday);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    if (age >= 15) {
        return age;
    }
    alert("Give a proper Age!");
    error = -1;

    return -1;
}
function CheckCity() {
    var city = document.getElementById('city').value;
    //alert(city);
    if (city.length >= 2 && city.length < 50) {
        return city;
    } else {
        alert("City field is invalid!");
        error = -1;
        return -1;
    }
}

function sendLoginPost() {

    var username = document.getElementById('user').value;
    var password = document.getElementById('pass').value;
    /* reset local storage */
    localStorage.setItem('username', '');
    localStorage.setItem('password', '');

    if (username === '' || password === '') {
        alert("ERRO!");
    } else {
        alert("Request Ok!");
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'EchoLogin');
        xhr.onload = function () {
            if (xhr.status === 200) {

                document.getElementById('LoginContent')
                        .innerHTML = xhr.responseText;
                localStorage.setItem('username', username);
                localStorage.setItem('password', password);
                // OR IF INTPUT IS INSIDE FORM BETTER USER JQUERY SERIALIZER var listvalues = $("#form").serialize();*
                if (xhr.responseText.indexOf("OK") !== -1) {
                    // alert(xhr.responseText);
                    //alert("OK");
                    /*the response is valid */
                    window.location.assign("Logged.html");
                    // alert(localStorage.getItem('username'));
                    //alert(localStorage.getItem('password'));

                    //pase the value 
                }

            } else if (xhr.status !== 200) {
                alert('Request failed. Returned status of ' + xhr.status);
            }
        };
        //alert(username);
        //xhr.send('username='+username  + '&lastName=' + lName);

        xhr.send('username=' + username + '&password=' + password);
    }
}
function users() {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'EchoLogin');
    xhr.onload = function () {
        if (xhr.status === 200) {
            //alert(xhr.responseText);
            document.getElementById('Users')

                    .innerHTML = xhr.responseText;
        } else if (xhr.status !== 200) {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };
    xhr.send( );
}



function logout1() {


    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'Echo');
    xhr.onload = function () {
        if (xhr.status === 200) {

            document.getElementById('ajaxContent').innerHTML = xhr.responseText;
            document.getElementById('LoginContent').innerHTML = xhr.responseText;
            document.getElementById('Users')
                    .innerHTML = xhr.responseText;
            if (window.location.toString().indexOf("Logged.html") !== -1)
                window.location.assign("login.html");


        } else if (xhr.status !== 200) {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };
    xhr.send();
}






