package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.stream.Collectors;
import Login.test;
import cs359db.db.UserDB;
import cs359db.model.User;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

@WebServlet(name = "Echo", urlPatterns = {"/Echo"})
public class Echo extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
          
        } catch(Exception e){
                 
                  
        }
        String Query = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        
        res.setContentType("text/html");

        try (PrintWriter o = res.getWriter()) {
            test.currentusername = null;
      
            if (Query.length() != 0) {
                /* de katafera na kanw get request, sinexeia m petouse error 405 zzzzzzzz*/
               
                    String GetName = DecodeData(Query);
             

            } else {
                /* remove cookies! */
                CookieExists(res, req.getCookies());
            }
        }
    }

    /**
     * @param data
     * @param str The queries from post method,we split them and put them in a
     * Map (key,value)
     * @return The Map
     */
    public String DecodeData(String str)  {
        //HashMap<String, HashMap<String, String>> data = new HashMap<String, HashMap<String, String>>();
        HashMap<String, String> user = new HashMap<>();
           
        int username = 0;
        String[] tokens = str.split("&");
        for (String t : tokens) {
            String[] parts = t.split("=");

            if (parts.length == 1) {
                user.put(parts[0], parts[1]);
            } else {

                user.put(parts[0], parts[1]);
            }

        }
   
        User usr = new User();
        usr.setUserName(user.get("username"));
        usr.setEmail(user.get("email"));
        usr.setPassword(user.get("password"));
        usr.setFirstName(user.get("firstname"));
        usr.setLastName(user.get("lastName"));
        usr.setBirthDate(user.get("birthday"));
        usr.setCountry(user.get("country"));
        usr.setTown(user.get("city"));
        usr.setGender(user.get("gender"));
        usr.setInfo(user.get("info"));
      
        /* current mail and username do not already exist so we add the user */
        try {
            
            if (UserDB.checkValidEmail(usr.getEmail()) && UserDB.checkValidUserName(usr.getUserName())) {
                UserDB.addUser(usr);
                        System.out.println("adding " + user.get("username"));
            }
        } catch (ClassNotFoundException classNotFoundException) {
        }


        return usr.getUserName();
    }

    private void CookieExists(HttpServletResponse res, Cookie[] cookies) {
        if (cookies != null) {
            for (Cookie c : cookies) {
                c.setValue(null);
                c.setMaxAge(0);
                res.addCookie(c);
            }
        }
        System.out.println("Cookies Removed");
    }
}
