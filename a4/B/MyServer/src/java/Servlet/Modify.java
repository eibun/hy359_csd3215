package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.stream.Collectors;
import Login.test;
import cs359db.db.UserDB;
import cs359db.model.User;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

@WebServlet(name = "Modify", urlPatterns = {"/Modify"})
public class Modify extends HttpServlet {

    public String username;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        res.setContentType("text/html");
        try (PrintWriter o = res.getWriter()) {

            String Query = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
            //String[] tokens = Query.split("\\|", 2);
            //split("&",2);
            //username = tokens[0];
            //  Query = tokens[1];
            res.setContentType("text/html");

            if (Query.length() != 0) {
                /* de katafera na kanw get request, sinexeia m petouse error 405 zzzzzzzz*/
                try {
                    String GetName = DecodeData(Query);
                } catch (Exception e) {

                }
                // res.sendRedirect("LogedIn.html");
                //req.getRequestDispatcher("LogedIn.html").include(req, res);  
                /*  for (HashMap.Entry<String, HashMap<String, String>> entry : test.params.entrySet()) {

                    String key = entry.getKey();
                    o.println("@: " + key);

                    for (HashMap.Entry<String, String> var : entry.getValue().entrySet()) {
                        String key2 = var.getKey();
                        String value2 = var.getValue();
                        o.println("<br>");
                        o.println("---> " + key2 + " = " + value2);
                        o.println("<br>");
                    }
             

            }*/
            }
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        res.setContentType("text/html");

        try (PrintWriter o = res.getWriter()) {
            try {
                o.println(test.print(1, test.currentusername));
            } catch (Exception e) {

            }
        }
    }

    /**
     * @param data
     * @param str The queries from post method,we split them and put them in a
     * Map (key,value)
     * @return The Map
     */
    public String DecodeData(String str) throws ClassNotFoundException {
        //HashMap<String, HashMap<String, String>> data = new HashMap<String, HashMap<String, String>>();
        HashMap<String, String> user = new HashMap<>();

        String[] tokens = str.split("&");
        for (String t : tokens) {
            String[] parts = t.split("=");

            if (parts.length == 1) {
                user.put(parts[0], parts[1]);
            } else {

                user.put(parts[0], parts[1]);
            }

        }
        User usr = UserDB.getUser(user.get("username"));
        // String mail =UserDB.getUser(user.get("username")).getEmail();// data.get(user.get("username")).get("email");
        
    
        usr.setPassword(user.get("password"));
        usr.setFirstName(user.get("firstname"));
        usr.setLastName( user.get("lastName"));
        usr.setBirthDate(user.get("birthday"));
        usr.setCountry(user.get("country"));
        usr.setTown(user.get("city"));
        usr.setGender(user.get("gender"));
        usr.setInfo(user.get("info"));
        UserDB.updateUser(usr);
         

        //String username=data.get("username").get("email");
        //data.put(user.get("username"), user);

        return user.get("username");
    }

}
