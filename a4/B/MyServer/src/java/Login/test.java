package Login;

import cs359db.db.UserDB;
import cs359db.model.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author nubie
 */
public class test {

    public static String currentusername;


    /*
    
    mode = 0 prints only username and email for the signed up users
    mode !=0 prints all the fields of the hashmap
    
     */
    public static String print(int mode, String name) throws ClassNotFoundException {
        String sb = "";
        if (mode == 0) {
            sb += ("<H1>Subscribed users</h1>");
            sb += ("<table border=\"1\">");
            sb += ("<tr>");
            sb += ("<td><b>Username</b></td>");
            sb += ("<td><b>Email</b></td>");
            sb += ("</tr>");
        }
        /*else if (mode == 1) {

            sb += ("<H1>Subscribed users</h1>");

            sb += ("<table border=\"1\">");
            sb += ("<tr>");
            sb += ("<td><b>Username</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Email</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>First Name</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Last Name</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Birthday</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Country</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>City</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>info</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Password</b></td>");
            sb += ("</tr>");
            sb += ("<tr>");
            sb += ("<td><b>Gender</b></td>");
            sb += ("</tr>");

            sb += ("</table>");
            sb += ("</br>");

        }*/
        List<User> users = UserDB.getUsers();
        for (int i = 0; i < users.size(); i++) {
            User u = users.get(i);
            if (mode == 0) {
                sb += ("<tr>");
                sb += ("<td>" + u.getUserName() + "</td>");

                sb += ("<td>" + u.getEmail() + "</td>");
                sb += ("</tr>");

            } else {
                if (u.getUserName().equals(name)) {
                    sb += (" <H1>Subscribed users</h1>");
                    sb += ("<table border=\"2\">");
                    sb += ("<tr>");
                    sb += ("<td>" + "Username: " + u.getUserName() + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Email " + u.getEmail() + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "First Name: " + u.getFirstName() + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Last Name: " + u.getLastName() + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Age: " + u.getBirthDate() + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Country: " + u.getCountry() + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "City: " + u.getTown() + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");

                    sb += ("<td>" + "Info: " + u.getInfo() + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Password: " + u.getPassword() + "</td>");
                    sb += ("</tr>");
                    sb += ("<tr>");
                    sb += ("<td>" + "Gender: " + u.getGender() + "</td>");
                    sb += ("</tr>");

                }
            }
        }
        sb += ("</table>");
        sb += ("</br>");
        return sb;

    }
}
