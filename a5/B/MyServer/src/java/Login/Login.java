/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Login;

import cs359db.db.UserDB;
import cs359db.model.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

/**
 *
 * @author nubie
 */
@WebServlet(name = "EchoLogin", urlPatterns = {"/EchoLogin"})
public class Login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            if ("POST".equalsIgnoreCase(request.getMethod())) {
                String query=request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
                /*delete all users */
                if (query.equals("del")) {
                    for (int i = 0; i < UserDB.getUsers().size(); i++) {
                        UserDB.deleteUser(UserDB.getUsers().get(i).getUserName());
                    }
                    out.println("Deleted!");
                    System.out.println("Deleted!!!!!!!!!!!!");
                } else {
                    System.out.println(query);
                    HashMap<String, String> user = DecodeData(query);
                    boolean founduser = false;

                    for (int i = 0; i < UserDB.getUsers().size(); i++) {
                        User u = UserDB.getUsers().get(i);
                        System.out.println(" Entry :" + u.getUserName() + " User :" + user.get("username"));
                        if (u.getUserName().equals(user.get("username"))) {
                            System.out.println("Okay equals");

                            if (u.getPassword().equals(user.get("password"))) {
                                System.out.println(" Inside");
                                InsertCookie(response, user.get("username"));
                                out.println("OK");
                                test.currentusername = user.get("username");
                                /*Create Cookies*/
                                //  
                                founduser = true;
                                break;

                            }
                        }
                    }

                    if (founduser == false) {
                        out.println("User :" + user.get("username") + " not found!");
                    } else {
                        // out.println("Cookie Added :D!");
                        // out.println("<br>");
                    }
                }
            } else {/*Get Method */


                Cookie[] cookies = request.getCookies();
                if (cookies != null) {
                    for (int i = 0; i < cookies.length; i++) {
                        Cookie c = cookies[i];

                        for (int z = 0; i < UserDB.getUsers().size(); z++) {
                            User u = UserDB.getUsers().get(z);
                            System.out.println("Entry key :" + u.getUserName() + " Value : " + c.getValue());
                            if (u.getUserName().equals(c.getValue())) {
                                out.println(c.getValue() + " : " + c.getName());
                                out.println(test.print(0, null));

                                break;
                            }

                        }
                    }
                }

            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception e) {

        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception e) {

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public HashMap<String, String> DecodeData(String str) {
        //HashMap<String, HashMap<String, String>> data = new HashMap<String, HashMap<String, String>>();
        HashMap<String, String> user = new HashMap<String, String>();
        String[] tokens = str.split("&");
        for (String t : tokens) {
            String[] parts = t.split("=");

            System.out.println("adding :" + parts[0] + " = " + parts[1]);
            user.put(parts[0], parts[1]);

        }
        return user;
    }

    private void InsertCookie(HttpServletResponse response, String c_name) {
        Cookie firstName = new Cookie("username", c_name);

        // Set expiry date after 24 Hrs for both the cookies.
        firstName.setMaxAge(60 * 60 * 24);
        // Add both the cookies in the response header.
        response.addCookie(firstName);
    }

}
