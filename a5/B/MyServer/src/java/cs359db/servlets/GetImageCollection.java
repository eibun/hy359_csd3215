/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs359db.servlets;

import cs359db.db.PhotosDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nubie
 */
@WebServlet("/GetImageCollection")
public class GetImageCollection extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Integer> photos;
        int i;
        String Start, End, json;
        Start = "[";
        End = "]";
        json = Start;
        i = 0;
        HashMap<String, String> Query;
        Query = DecodeData(request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));

        try {

            if (Query.get("userName").equals("null")) {
                /* no user */
                photos = PhotosDB.getPhotoIDs(10);
            } else {/*found the user */
                System.out.println("|"+Query.get("number")+"|");
                
                photos = PhotosDB.getPhotoIDs(Integer.decode((Query.get("number"))) , Query.get("userName"));
            }
            for (; i < photos.size(); i++) {
                json = json + "!id" + photos.get(i) + "!";
                if (i != photos.size() - 1) {
                    json = json + ", ";
                }
            }
            json = json.replace('!', '"');
            json = json + End;
            System.out.println(json);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GetImageCollection.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.setContentType("text/html");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println(json);

        }

    }

    public HashMap<String, String> DecodeData(String str) {
        //HashMap<String, HashMap<String, String>> data = new HashMap<String, HashMap<String, String>>();
        HashMap<String, String> user = new HashMap<>();

        int username = 0;
        String[] tokens = str.split("&");
        for (String t : tokens) {
            String[] parts = t.split("=");

            user.put(parts[0], parts[1]);

        }
        return user;
    }
}
