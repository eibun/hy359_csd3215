/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs359db.servlets;

import cs359db.db.PhotosDB;
import cs359db.model.Photo;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nubie
 */
@WebServlet(name = "GetImage", urlPatterns = {"/GetImage"})
public class GetImage extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HashMap<String, String> Query;
        Query = DecodeData(request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
        Photo meta = null;
        try {

            meta = PhotosDB.getPhotoMetadataWithID(Integer.parseInt(Query.get("image")));

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GetImage.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (Query.get("metadata").equals("true")) {
            try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                System.out.println(meta.toString());
                if (meta != null) {
                    out.println(meta.toString());
                }
            }
        } else {

            //This is what you should do for the response in the servlet
            if (meta != null) {
                response.setContentType("application/octet-stream");   // Use the appropriate type from the metadata

                // Get the blob of the photo
                byte[] imgData = null;

                try {
                    imgData = PhotosDB.getPhotoBlobWithID(Integer.parseInt(Query.get("image")));

                    OutputStream os = response.getOutputStream();
                    
                    os.write(imgData);
                    os.flush();
                    os.close();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(GetImage.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }

    }

    public HashMap<String, String> DecodeData(String str) {

        HashMap<String, String> user = new HashMap<>();

        String[] tokens = str.split("&");
        for (String t : tokens) {
            String[] parts = t.split("=");

            user.put(parts[0], parts[1]);

        }
        return user;
    }
}
