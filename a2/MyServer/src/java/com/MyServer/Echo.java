package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Echo", urlPatterns = { "/Echo" })
public class Echo extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String htmlHeader = "<html><head><title>Echo Request</title></head><body>";
		String htmlFooter = "</body></html>";
		res.setContentType("text/html");
		try (PrintWriter o = res.getWriter()) {

			if ("POST".equalsIgnoreCase(req.getMethod())) {

				HashMap<String, String> params = String(
						req.getReader().lines().collect(Collectors.joining(System.lineSeparator())).toString());
				for (HashMap.Entry<String, String> entry : params.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue();
					o.println("<br>");
					o.println(key + " = " + value);
				}
			}
			o.println(htmlFooter);
		}
	}

	/**
	 * @param str
	 *            The queries from post method,we split them and put them in a
	 *            Map (key,value)
	 * @return The Map
	 */
	public static HashMap<String, String> String(String str) {
		HashMap<String, String> data = new HashMap<String, String>();
		System.out.println("HERE :" + str);
		String[] tokens = str.split("&");
		for (String t : tokens) {
			String[] parts = t.split("=");
	 
			if (parts.length==1) {
				data.put(parts[0], " ");
			} else
				data.put(parts[0], parts[1]);

		}
		return data;
	}

}