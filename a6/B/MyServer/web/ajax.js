//rikas autismo
var error = 0;
var ImageArray = [];
var GPSlon = [];
var GPSlat = [];
var ExifData = [];
var info = "<button  class=button id=toggleButton \n\
type=\"button\" onclick=toggleText()>Click Me!<\/button>";
var status = "more";
var currentIndex;
var BiggerImage = 0;
var CollapseButton = 0; /* not created */

String.prototype.replaceAt = function (index, character) {
    return this.substr(0, index) + character + this.substr(index + character.length);
};

function encryption(p) {
    var i = 0;
    for (var i = 0; i < p.length; i++) {
        var l = String.fromCharCode(p.charCodeAt(i) + 1);
        p = p.replaceAt(i, l);
    }
    return p;
}

function sendAjaxPOST() {

    error = 0;
    var username = CheckUserName();
    var fname = CheckName(3, 'fname');
    var lname = CheckName(4, 'lname');
    var email = CheckEmail();
    var password = CheckPassWord('password');
    var password2 = CheckPassWord('password2');
    if (password !== password2 || password === -1) {/* passwords match! */
        alert("Retype your Passwords!");
        error = -1;
    }
    var bday = CheckAge();

    var gender = findGender();

    var city = CheckCity();
    var country = document.getElementById("countries");
    country = country.options[country.selectedIndex].text;
    var info = document.getElementById('info').value;

    // alert(country);
    //alert(error);
    if (error < 0) {
        alert("Retype your information!");
    } else {
        alert("Your data are correct");


        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'Echo');
        xhr.onload = function () {
            if (xhr.status === 200) {

                document.getElementById('ajaxContent').innerHTML = xhr.responseText;

            } else if (xhr.status !== 200) {

                alert('Request failed. Returned status of ' + xhr.status);
            }
        };
        //alert(username);
        //xhr.send('username='+username  + '&lastName=' + lName);
        xhr.send('username=' + username + '&email=' + email +
                '&password=' + encryption(password) + '&password2=' + encryption(password2) + '&firstname=' + fname +
                '&lastName=' + lname + '&birthday=' + bday + '&gender=' + gender + '&city=' + city + '&country=' + country + '&info=' + info);
    }
}

function MyInfo() {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'Modify');
    xhr.onload = function () {
        if (xhr.status === 200) {
            //alert(xhr.responseText);
            document.getElementById('myinfo')

                    .innerHTML = xhr.responseText;
        } else if (xhr.status !== 200) {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };

    xhr.send();

}

function Redirect_to_login() {
    window.location = "http://localhost:8084/MyServer/login.html";
}

function DeleteUsers() {

    alert("DeleteUsers was called.");
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'EchoLogin');
    xhr.onload = function () {
        if (xhr.status === 200) {
            alert(xhr.responseText);
            document.getElementById('Delete1')
                    .innerHTML = xhr.responseText;
            Redirect_to_login();
        } else if (xhr.status !== 200) {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };

    xhr.send("del");


}

function DeleteUsersPhoto() {

    alert("DeleteUsersPhoto was called.");
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'EchoLogin');
    xhr.onload = function () {
        if (xhr.status === 200) {
            alert(xhr.responseText);
            document.getElementById('Delete2')
                    .innerHTML = xhr.responseText;
        } else if (xhr.status !== 200) {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };

    xhr.send("delph");


}

function EditUser() {
    error = 0;
    var fname = CheckName(3, 'fname');
    var lname = CheckName(4, 'lname');
    var password = CheckPassWord('password');
    var password2 = CheckPassWord('password2');
    if (password !== password2 || password === -1) {/* passwords match! */
        alert("Retype your Passwords!");
        error = -1;
    }
    var bday = CheckAge();

    var gender = findGender();

    var city = CheckCity();
    var country = document.getElementById("countries");
    country = country.options[country.selectedIndex].text;
    var info = document.getElementById('info').value;

    // alert(country);
    //alert(error);
    if (error < 0) {
        alert("Retype your information!");
    } else {
        alert("Your data are correct");


        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'Modify');
        xhr.onload = function () {
            if (xhr.status === 200) {
                document.getElementById('myinfo').innerHTML = '';
                document.getElementById('ajaxContent').innerHTML = xhr.responseText;
            } else if (xhr.status !== 200) {
                alert('Request failed. Returned status of ' + xhr.status);
            }
        };
        //alert(username);
        //xhr.send('username='+username  + '&lastName=' + lName);

        xhr.send('username=' + localStorage.getItem('username') + '&password=' + encryption(localStorage.getItem('password')) + '|&password=' + encryption(password) + '&password2=' + encryption(password2) + '&firstname=' + fname +
                '&lastName=' + lname + '&birthday=' + bday + '&gender=' + gender + '&city=' + city + '&country=' + country + '&info=' + info);
    }
}
function findGender() {
    if (document.getElementById('male').checked) {
        return "male";
    } else if (document.getElementById('female').checked) {
        return "female";
    } else {
        return "na";
    }
}
function CheckUserName() {
    var username = document.getElementById('username').value;
    if (username.length <= 7) {
        alert("Give a username of at least 8 Characters!");
        error = -1;
        return -1;
    } else {
        return username;
    }


}
function CheckEmail() {
    var email = document.getElementById('email').value;
    var atcount = (email.match(/@/) || []).length;
    if (atcount === 1) {/*only 1 @ */
        var dotcount = (email.match(/./) || []).length;
        if (dotcount >= 1) {/*only 1 dot*/
            return email;
        } else
            alert("Give a Proper email address!");
    } else
        alert("Give a Proper email address!");
    error = -1;
}
function CheckPassWord(current) {
    var password = document.getElementById(current).value;
    if (password.length > 5 && password.length < 10) {/* valid length */

        if (/\d/.test(password)) {/*password contains a number */
            if (/[a-zA-Z]/.test(password)) {

                if (/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/.test(password))/* password contains special characters */
                    return password;
                else
                    alert("Password must contain at least one special symbol!");
            } else
                alert("Pass must contain chars!");
        } else {
            alert("Password must have a number!");
        }


    } else {
        error = -1;
        alert("Password Length must be > 6 and < 10");
        return -1;
    }
}
function CheckName(min, elem) {
    var field = document.getElementById(elem).value;
    if (field.length >= min && field.length < 20) {/* valid length */
        return field;
    } else {
        error = -1;
        if (min === 3) {
            alert("First name must be >3 and < 20");
        } else {
            alert("Last name must be >4 and < 20");
        }

        return -1;
    }


}
function CheckAge() {
    var bday = document.getElementById('birthday').value;
    var today = new Date();
    var birthDate = new Date(bday);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    if (age >= 15) {
        return age;
    }
    alert("Give a proper Age!");
    error = -1;

    return -1;
}
function CheckCity() {
    var city = document.getElementById('city').value;
    //alert(city);
    if (city.length >= 2 && city.length < 50) {
        return city;
    } else {
        alert("City field is invalid!");
        error = -1;
        return -1;
    }
}

function sendLoginPost() {

    var username = document.getElementById('user').value;
    var password = document.getElementById('pass').value;
    /* reset local storage */
    localStorage.setItem('username', '');
    localStorage.setItem('password', '');

    if (username === '' || password === '') {
        alert("ERROR!");
    } else {
        //alert("Request Ok!");
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'EchoLogin');
        xhr.onload = function () {
            if (xhr.status === 200) {

                document.getElementById('LoginContent')
                        .innerHTML = xhr.responseText;
                localStorage.setItem('username', username);
                localStorage.setItem('password', password);
                // OR IF INTPUT IS INSIDE FORM BETTER USER JQUERY SERIALIZER var listvalues = $("#form").serialize();*
                if (xhr.responseText.indexOf("OK") !== -1) {
                    // alert(xhr.responseText);
                    //alert("OK");
                    /*the response is valid */
                    window.location.assign("Logged.html");
                    // alert(localStorage.getItem('username'));
                    //alert(localStorage.getItem('password'));

                    //pase the value
                }

            } else if (xhr.status !== 200) {
                alert('Request failed. Returned status of ' + xhr.status);
            }
        };
        //alert(username);
        //xhr.send('username='+username  + '&lastName=' + lName);

        xhr.send('username=' + username + '&password=' + encryption(password));
    }
}
function users() {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'EchoLogin');
    xhr.onload = function () {
        if (xhr.status === 200) {
            alert(xhr.responseText);
            document.getElementById('Usersz')
                    .innerHTML = xhr.responseText;
            document.getElementById('Usersz')
                    .innerHTML += "<br>Give name of username\n\
                    you want to see his info: <input \n\
                    id=\"unamer\" type=\"text\" name=\"unamer\" size=\"10\">\n\
<button type=\"button\" onclick=\"showUserByDemand()\">Show this user!</button>";
        } else if (xhr.status !== 200) {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };
    xhr.send( );

}

function showUserByDemand() {

    var metadata;
    var xhr = new XMLHttpRequest();
    var formData = new FormData();
    var username = document.getElementById('unamer').value;
    var number = 10;

    document.getElementById('imgInfo').innerHTML = "<br><br>";
    document.getElementById('container4').innerHTML = "<br><br>";
    metadata = false;

    var array;
    xhr = new XMLHttpRequest();
    xhr.open('POST', 'GetImageCollection');
    xhr.onload = function () {

        if (xhr.status === 200) {
            //alert("Send request!");
            /*pirame to response apo to server,ora gia na paroume tis eikones me
             * nea requests
             */
            var p = xhr.responseText;
            // p = p.replace(/,/g, '');
            p = p.replace(/"/g, '');
            p = p.replace('[', '');
            p = p.replace(']', '');
            /* telos parsing,exoume to array me ta id,stelnoume requests*/
            array = p.split(',');
            if (metadata === false) {
                for (var i = 0; i < array.length; i++)
                    GetImageBlobImage(array[i].replace('id', '').replace(/\s+/g, ''));
                for (var i = 0; i < array.length; i++)
                    GetImageBlobInfo(array[i].replace('id', '').replace(/\s+/g, ''));
            } else {
                for (var i = 0; i < array.length; i++)
                    GetImageBlobInfo(array[i].replace('id', '').replace(/\s+/g, ''));
            }
            // document.getElementById('images').innerHTML = p;

        } else {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };
    alert("userName=" + username + "&" + "number=" + number);
    xhr.send("userName=" + username + "&" + "number=" + number);
    // if number is -1 server then user did not give value
}

function logout1() {

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'Echo');
    xhr.onload = function () {
        if (xhr.status === 200) {

            document.getElementById('ajaxContent').innerHTML = xhr.responseText;
            document.getElementById('LoginContent').innerHTML = xhr.responseText;
            document.getElementById('Users')
                    .innerHTML = xhr.responseText;
            if (window.location.toString().indexOf("Logged.html") !== -1)
                window.location.assign("login.html");


        } else if (xhr.status !== 200) {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };
    xhr.send();
}

function PhotosPremium() {

    alert("Uploading Image");

    var formData = new FormData();
    var file = document.getElementById('file').files[0];
    var username = document.getElementById('usern').value;
    var content = document.getElementById('content').value;
    var title = document.getElementById('title').value;

    formData.append('userName', username);
    formData.append('contentType', content);
    formData.append('title', title);
    formData.append('photo', file);

    xhr = new XMLHttpRequest();
    xhr.open('POST', 'UploadImage');
    xhr.onload = function () {

        if (xhr.status === 200) {
            alert("Image Uploaded");
        } else {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };
    xhr.send(formData);
}

function PrintPhotos() {

    var metadata;
    var xhr = new XMLHttpRequest();
    var formData = new FormData();
    var username = localStorage.getItem('username');
    var number = parseInt(document.getElementById('number').value);
    if (!Number.isInteger(number)) {
        number = 10;
    }

    if (document.getElementById('true').checked) {
        document.getElementById('imgInfo').innerHTML = "<br><br>";
        metadata = true;
    } else if (document.getElementById('false').checked) {
        document.getElementById('container4').innerHTML = "<br><br>";
        metadata = false;
    }

    var array;
    xhr = new XMLHttpRequest();
    xhr.open('POST', 'GetImageCollection');
    xhr.onload = function () {

        if (xhr.status === 200) {
            //alert("Send request!");
            /*pirame to response apo to server,ora gia na paroume tis eikones me 
             * nea requests 
             */
            var p = xhr.responseText;
            // p = p.replace(/,/g, '');
            p = p.replace(/"/g, '');
            p = p.replace('[', '');
            p = p.replace(']', '');
            /* telos parsing,exoume to array me ta id,stelnoume requests*/
            array = p.split(',');
            if (metadata === false) {
                for (var i = 0; i < array.length; i++)
                    GetImageBlobImage(array[i].replace('id', '').replace(/\s+/g, ''));
                for (var i = 0; i < array.length; i++)
                    GetImageBlobInfo(array[i].replace('id', '').replace(/\s+/g, ''));
            } else {
                for (var i = 0; i < array.length; i++)
                    GetImageBlobInfo(array[i].replace('id', '').replace(/\s+/g, ''));
            }
            // document.getElementById('images').innerHTML = p;

        } else {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };
    xhr.send("userName=" + username + "&" + "number=" + number);
    // if number is -1 server then user did not give value
}

function PrintPapadakosPhotos() {

    var xhr = new XMLHttpRequest();
    var formData = new FormData();
    var username = localStorage.getItem('username');

    if (document.getElementById('true').checked) {
        document.getElementById('imgInfo').innerHTML = "<br><br>";
        metadata = true;
    } else if (document.getElementById('false').checked) {
        document.getElementById('imgInfo').innerHTML = "<br><br>";
        document.getElementById('container4').innerHTML = "<br><br>";
        metadata = false;
    }

    var array;
    xhr = new XMLHttpRequest();
    xhr.open('POST', 'GetImageCollection');
    xhr.onload = function () {

        if (xhr.status === 200) {
            //alert("Send request!");
            /*pirame to response apo to server,ora gia na paroume tis eikones me
             * nea requests
             */
            var p = xhr.responseText;
            // p = p.replace(/,/g, '');
            p = p.replace(/"/g, '');
            p = p.replace('[', '');
            p = p.replace(']', '');
            /* telos parsing,exoume to array me ta id,stelnoume requests*/
            array = p.split(',');
            if (metadata === false) {
                for (var i = 0; i < array.length; i++)
                    GetImageBlobImage(array[i].replace('id', '').replace(/\s+/g, ''));
                for (var i = 0; i < array.length; i++)
                    GetImageBlobInfo(array[i].replace('id', '').replace(/\s+/g, ''));
            } else {
                for (var i = 0; i < array.length; i++)
                    GetImageBlobInfo(array[i].replace('id', '').replace(/\s+/g, ''));
            }

            //document.getElementById('images').innerHTML = p;

        } else {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };
    xhr.send("userName=null&" + "number=" + -1);
}

function GetImageBlobInfo(id) {
    //alert("id = " + id);
    var xhr = new XMLHttpRequest();

    /*an boreis eksasfalise oti to pedio number einai sigoura arithmos */
    /*if(!Number.isInteger(id)){
     alert("error: corrupt id\n");
     return;
     }*/

    xhr.open('POST', 'GetImage');

    xhr.onload = function () {
        if (xhr.status === 200) {

            //alert(xhr.responseText);
            document.getElementById('imgInfo').innerHTML += "<br><br>";
            document.getElementById('imgInfo').innerHTML += "<td>" + xhr.responseText + "</td>";
            //document.getElementById('images').innerHTML = xhr.responseText;

            /*
             * an boreseis ftiakse to retun apo tin getimage gia
             * tin peritpwsi pou to radio buttton no eiani pressed,
             * auto simainei oti prepei na deiskei eikona ston client
             */

            // document.getElementById('images').innerHTML = xhr.responseText;

        } else if (xhr.status !== 200) {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };
    // alert(id);
    xhr.send("image=" + id + "&metadata=" + true);
}

function GetImageBlobImage(id) {
    //alert("id = " + id);
    var xhr = new XMLHttpRequest();

    /*an boreis eksasfalise oti to pedio number einai sigoura arithmos */
    /*if(!Number.isInteger(id)){
     alert("error: corrupt id\n");
     return;
     }*/

    xhr.responseType = 'arraybuffer';

    xhr.open('POST', 'GetImage');

    xhr.onload = function () {
        if (xhr.status === 200) {

            //alert(xhr.responseXML);

            //console.log("2> " + xhr.response);
            //  console.log("2b> "+ xhr.responseText);

            var uInt8Array = new Uint8Array(this.response);
            var i = uInt8Array.length;
            var biStr = new Array(i);
            while (i--)
            {
                biStr[i] = String.fromCharCode(uInt8Array[i]);
            }
            var data = biStr.join('');
            var base64 = window.btoa(data);
            // document.getElementById('myImage').src = "data:image/png;base64," + base64;
            //console.log("3> " + base64);

            var span = document.createElement('div');
            span.setAttribute('class', 'tiv');
            ImageArray[id] = ("data:image/png;base64," + base64);
            span.setAttribute("onclick", 'showImage(\'' + id + '\')');

            span.innerHTML = ['<img  src="', "data:image/png;base64," + base64,
                '" title="', "hallo",
                '">'
            ].join('');
                
                 span.innerHTML += ["<br><br><br><br><br><br><br><br><br><br>\n\
                <br><br><br><br><br><select id=\"voteNum"+id+"\" >\n\
                <option value=\"1\">1</option> \n\
                <option value=\"2\">2</option>\n\
                <option value=\"3\">3</option> \n\
                <option value=\"4\">4</option>\n\
                <option value=\"5\">5</option></select>\n\
                    <button type=\"vote\" onclick=\"VoteCall("+id+")\">Vote!</button> "].join('');
            //file.z=file.webkitRelativePath;
            //alert(file.z);
            //console.log(getExif(file.z));
            document.getElementById('container4').insertBefore(span, null);

            /*
             
             document.getElementById('container1').innerHTML +=
             "<img style='display:block; width:100px;height:100px;'\n\
             id='base64image'src='data:image/jpeg;base64,\n\
             '" + base64 + "'/>";
             document.getElementById('container1').innerHTML += "<br>";
             
             */

            /*
             * an boreseis ftiakse to retun apo tin getimage gia
             * tin peritpwsi pou to radio buttton no eiani pressed,
             * auto simainei oti prepei na deiskei eikona ston client
             */

            // document.getElementById('images').innerHTML = xhr.responseText;

        } else if (xhr.status !== 200) {
            alert('Request failed. Returned status of ' + xhr.status);
        }
    };
    // alert(id);
    xhr.send("image=" + id + "&metadata=" + false);
}

function VoteCall(id){
    vote = document.getElementById("voteNum"+id).value;
    var xhr = new XMLHttpRequest();
        xhr.open('POST', 'Rating');
        xhr.onload = function () {
            if (xhr.status === 200) {
                    alert(xhr.responseText);

            } else if (xhr.status !== 200) {

                alert('Request failed. Returned status of ' + xhr.status);
            }
        };
        //alert(username);
        //xhr.send('username='+username  + '&lastName=' + lName);
        xhr.send("id="+id+"&rating="+vote);
        
}

function showImage(id) {
    //alert(BiggerImage);
    var elem = 'container1';
    var span2 = document.createElement('div');
    var child, parent = document.getElementById(elem);
    if (BiggerImage === 1) {

        child = document.getElementById("tiv2");
        parent.removeChild(child);
        document.getElementById('map').innerHTML = '';
        //document.getElementById("allMetaDataSpan").innerHTML = '';
        BiggerImage = 0;
        ExifData[currentIndex] = '';
    }
    if (CollapseButton === 0) {
        /*CollapseButton created */
        span2 = document.createElement('div');
        span2.setAttribute('id', 'collapseb');
       
        span2.innerHTML = info;

        document.getElementById(elem).insertBefore(span2, null);
        CollapseButton = 1;
    }

    document.getElementById("toggleButton").innerText = "See Less";
    status = "more";
    span = document.createElement('div');
    span.innerHTML = ['<img id ="', "Item" + id, '"src="', ImageArray[id],
        '" title="', "lol", '">'].join('');
    span.setAttribute('id', 'tiv2');

    span.innerHTML = span.innerHTML + ["<pre id=" + "\"allMetaDataSpan\"" +
                "></pre>"
    ].join('');
    document.getElementById("container1").insertBefore(span, null);
    BiggerImage = 1;

    showImageDetailedExifInfo(id, "container1");

}

function showImageDetailedExifInfo(index, elem) {

    getExif1(index);

    initMap(index, elem);
    currentIndex = index;
    ExifData[index] = document.getElementById("allMetaDataSpan").innerHTML;
}

var toDecimal = function (number) {

    "use strict";
    return number[0].numerator + number[1].numerator / (60 * number[1].denominator) +
            number[2].numerator / (3600 * number[2].denominator);
};
function toggleText() {
    "use strict";
    if (status === "less") {
        document.getElementById("allMetaDataSpan").innerHTML = ExifData[currentIndex];
        document.getElementById("toggleButton").innerText = "See Less";
        status = "more";
    } else if (status === "more") {
        document.getElementById("allMetaDataSpan").innerHTML =
                "Click above to Show More";
        document.getElementById("toggleButton").innerText = "See More";
        status = "less";
    }
}
function initMap(index, elem) {

    var uluru, map, marker;
    uluru = {
        lat: GPSlat[index],
        lng: GPSlon[index]
    };
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: uluru
    });
    marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}

function getExif1(index) {
    "use strict";

    var element = "Item" + index, img2, lon, lat, allMetaData, allMetaDataSpan;
    img2 = document.getElementById(element);
    //alert(img2);
    EXIF.getData(img2, function () {

        lon = toDecimal(EXIF.getTag(this, 'GPSLongitude'));
        lat = toDecimal(EXIF.getTag(this, 'GPSLatitude'));
        GPSlon[index] = lon;
        GPSlat[index] = lat;

        allMetaData = EXIF.getAllTags(this);
        allMetaDataSpan = document.getElementById("allMetaDataSpan");
        allMetaDataSpan.innerHTML = JSON.stringify(allMetaData,
                null, "\t");

    });

}
