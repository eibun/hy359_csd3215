/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs359db.servlets;

import Login.test;
import cs359db.db.RatingDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nubie
 */
@WebServlet(name = "Rating", urlPatterns = {"/Rating"})
public class Rating extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     *
     */
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try (PrintWriter out = response.getWriter()) {

            HashMap<String, String> Query;
            Query = DecodeData(request.getReader().lines().collect(Collectors.joining(System.lineSeparator())));
            int score = Integer.parseInt(Query.get("rating"));
            int id = Integer.parseInt(Query.get("id"));
            String username = test.currentusername;
            cs359db.model.Rating rating = new cs359db.model.Rating();
            rating.setPhotoID(id);
            int size = 0;
            try {
                size = RatingDB.getRatings(id).size();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Rating.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (size == 0) {
                try {
                    rating.setRate(score);

                    RatingDB.addRating(rating);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Rating.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                try {
                    if (RatingDB.getRatings().get(id).getUserName().equals(username)) {
                        rating.setRate(score);
                        RatingDB.updateRating(rating);
                    }
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Rating.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            float value=0.0f;
            for (int i=0;i<size;i++){
                try {
                    value+=RatingDB.getRatings(id).get(i).getRate();
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Rating.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            value /=size;
            out.println("Final Rate =:" + value);
        }
    }

    public HashMap<String, String> DecodeData(String str) {
        HashMap<String, String> user = new HashMap<>();

        String[] tokens = str.split("&");
        for (String t : tokens) {
            String[] parts = t.split("=");

            user.put(parts[0], parts[1]);

        }
        return user;
    }

}
